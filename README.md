# Sanity
sanity est un script réalisé pour avertir les nounous de la présence
d'inconsistence dans le ldap. Pour le moment, il ne rapporte que les machines
dont les clefs ssh ne sont pas renseigner dans le ldap.

## Déploiement

	1. Cloner le script dans `${HOME}/.local/services`
	2. Copier la configuration exemple et adpater là à vos besoins
	```bash
	$ cp sanity.example.json sanity.json
	$ edit sanity.json
	```
	3. Lier le service et le timer systemd
	```
	$ systemctl --user link ${PWD}/sanity.{service,timer}
	```
	4. Démarrer le timer
	```bash
	$ systemctl --user enable --now sanity.timer
	```
