#!/usr/bin/python3
import argparse
import getpass
import email.message
import functools
import jinja2
import json
import logging
import os
import smtplib
import subprocess
import sys

import jinja2
import ldap
import ldap.modlist


path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('addkeys')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description="Add SSH keys to crans LDAP")
	parser.add_argument('-s', '--ssh', type=str, help="SSH command to run", default='ssh-keyscan {}.adm.crans.org')
	parser.add_argument('-q', '--quiet', help="Supress warning and errors message", action='store_true')
	parser.add_argument('-H', '--host', action='append', type=str.lower, help="Hostnames to get ssh keys")
	subparsers = parser.add_subparsers(dest='mode')
	parser_mail = subparsers.add_parser('mail', description="Run in mail mode")
	parser_add = subparsers.add_parser('add', description="Run in add mode")
	args = parser.parse_args()

	if args.quiet:
		logger.setLevel(60)

	with open(os.path.join(path, 'sanity.json')) as file:
		config = json.load(file)

	if args.host:
		servers = args.host
	else:
		base = ldap.initialize(config['ldap']['server'])
		if config['ldap']['server'].startswith('ldaps://'):
			base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
			base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
		servers_id = base.search('ou=hosts,dc=crans,dc=org', ldap.SCOPE_ONELEVEL, 'objectClass=device')
		servers = base.result(servers_id)[1]
		servers = [
			{
				k: list(map(lambda s: s.decode('utf8'),v)) if k != 'description' else
					functools.reduce(lambda d,p: (d[1].setdefault(p[0], []).append(p[1]),d[1]), \
						   map(lambda x: x.decode('utf8').split(':',1), v),(None,{})
					)[1] for k,v in d.items()
			} for _,d in servers
		]

		blacklist = config.get('blacklist', [])

		servers = list(sorted(map(lambda server: server['cn'][0],
			filter(lambda server: all(cn not in blacklist for cn in server['cn']) and ('description' not in server or \
				all(k not in server['description'] for k in ['ssh-rsa','ecdsa-sha2-nistp256','ssh-ed25519'])),
		   servers))))

	keys = {}
	for host in servers:
		command = args.ssh.format(host).split(' ')
		host_keys = subprocess.run(command, capture_output=True).stdout.decode('utf-8')
		host_keys = list(filter(lambda key: len(key) == 3,
			(key.split() for key in host_keys.strip().split('\n')
				if any(keytype in key for keytype in ['ssh-rsa','ecdsa-sha2-nistp256','ssh-ed25519']))
		))
		if len(host_keys) == 0:
			logger.warning(f'Couldn\'t retrieve {host} ssh keys')
			continue
		for key in host_keys:
			keys.setdefault(host, {})[key[1]] = key[2]

	if args.mode == 'add':
		base = ldap.initialize(config['ldap']['server'])
		if config['ldap']['server'].startswith('ldaps://'):
			base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
			base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
		base.simple_bind_s(
			'uid={},ou=passwd,dc=crans,dc=org'.format(getpass.getuser()),
			getpass.getpass(f"Login to ldap as {getpass.getuser()} with password : ")
		)
		for host in keys:
			print(f"Add keys for {host} ? [Yn]", end="")
			if input().lower() not in [ '', 'Y', 'y' ]:
				continue
			host_query_id = base.search(f"cn={host},ou=hosts,dc=crans,dc=org", ldap.SCOPE_BASE, "objectClass=device")
			dn, entry = base.result(host_query_id)[1][0]
			descriptions = entry.get('descriptions', [])
			descriptions_old = { 'description': descriptions.copy() }
			for keytype,key in keys[host].items():
				if (key := f'{keytype}:{key}'.encode('utf-8')) not in descriptions:
					descriptions.append(key)
			descriptions = {'description': descriptions}
			if descriptions != descriptions_old:
				base.modify_s(dn, ldap.modlist.modifyModlist(descriptions_old, descriptions))
	elif args.mode == 'mail':
		if len(keys) > 0:
			msg = email.message.EmailMessage()
			with open(os.path.join(path, 'templates/mail.j2')) as file:
				template = jinja2.Template(file.read())
			content = template.render(keys=keys).strip()
			msg.set_content(content)
			for k,v in config.get('headers', {}).items(): msg[k] = v
			with smtplib.SMTP(config.get('smtp', 'localhost')) as smtp:
				smtp.send_message(msg)
	else:
		for host in keys:
			print(f'Key available for {host}')
